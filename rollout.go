package rollout

import (
	"errors"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type IncrementalRollout struct {
	Percentage int
}

func (r IncrementalRollout) Rollout() bool {
	result, err := Rollout(r.Percentage)

	if err != nil {
		panic("unexpected rollout error")
	}

	return result
}

func New(percentage int) (IncrementalRollout, error) {
	if percentage < 0 || percentage > 100 {
		return IncrementalRollout{}, errors.New("rollout value should be between 0 and 100 inclusive")
	}

	return IncrementalRollout{Percentage: percentage}, nil
}

// Rollout returns true and no error when during this run something should
// happen according to the likelyhood passed as a percentage value to this
// function. It returns false rollout and an error if the percentage value is
// negative or higher than 100.
func Rollout(percentage int) (bool, error) {
	if percentage < 0 || percentage > 100 {
		return false, errors.New("rollout value should be between 0 and 100 inclusive")
	}

	if percentage == 0 {
		return false, nil
	}

	if percentage == 100 {
		return true, nil
	}

	return rand.Intn(100) < percentage, nil
}
