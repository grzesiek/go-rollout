package rollout

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const samples = 100000

func TestNewIncrementalRollout(t *testing.T) {
	t.Run("when rollout is invalid", func(t *testing.T) {
		_, err := New(-10)

		assert.EqualError(t, err, "rollout value should be between 0 and 100 inclusive")
	})

	t.Run("when rollout is valid", func(t *testing.T) {
		rollout, err := New(15)
		require.NoError(t, err)

		assert.Equal(t, rollout.Percentage, 15)
	})
}

func TestIncrementalRollout(t *testing.T) {
	rollout, err := New(100)
	require.NoError(t, err)

	assert.True(t, rollout.Rollout())
}

func TestRollout(t *testing.T) {
	t.Run("when rollout is negative", func(t *testing.T) {
		_, err := avgRollout(-20)

		assert.EqualError(t, err, "rollout value should be between 0 and 100 inclusive")
	})

	t.Run("when rollout is larger than 100", func(t *testing.T) {
		_, err := avgRollout(101)

		assert.EqualError(t, err, "rollout value should be between 0 and 100 inclusive")
	})

	t.Run("when rollout is set to 0%", func(t *testing.T) {
		result, err := avgRollout(0)
		require.NoError(t, err)

		assert.Equal(t, 0, result)
	})

	t.Run("when rollout is set to 100%", func(t *testing.T) {
		result, err := avgRollout(100)
		require.NoError(t, err)

		assert.Equal(t, 100, result)
	})

	t.Run("when rollout is 20%", func(t *testing.T) {
		result, err := avgRollout(20)
		require.NoError(t, err)

		assert.InDelta(t, 20, result, 2)
	})

	t.Run("when rollout is 45%", func(t *testing.T) {
		result, err := avgRollout(45)
		require.NoError(t, err)

		assert.InDelta(t, 45, result, 2)
	})
}

func TestConcurrentRollout(t *testing.T) {
	results := &sync.Map{}
	wg := &sync.WaitGroup{}

	for i := 0; i < 100; i++ {
		wg.Add(1)

		go func() {
			result, err := avgRollout(65)
			require.NoError(t, err)

			results.Store(i, result)
			wg.Done()
		}()
	}

	wg.Wait()

	results.Range(func(key, value interface{}) bool {
		assert.InEpsilon(t, 65, value.(int), 0.04)

		return !t.Failed()
	})
}

func BenchmarkRollout(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Rollout(95)
	}
}

func avgRollout(p int) (int, error) {
	avg := 0.0

	for i := 0; i < samples; i++ {
		result, err := Rollout(p)

		if err != nil {
			return 0, err
		}

		if result {
			avg += 1
		}
	}

	return int((avg / float64(samples)) * 100), nil
}
