# go-rollout

Naively simple tool that makes it just a little easier to implement an
incremental rollout in Go.

It should be thread safe.

## Usage

```go
rollout, err := rollout.Rollout(20)
if err != nil {
 // Handle error
}

if rollout {
  // do something
}
```

## Tests

```bash
$ go test .
$ go test -bench .
```
